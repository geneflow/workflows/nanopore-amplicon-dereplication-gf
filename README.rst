=================================================
Nanopore Amplicon Dereplication GeneFlow Workflow
=================================================

Overview
========

This workflow takes basecalled, demultiplexed, and trimmed fastq files from Nanopore sequencing. It uses Canu to dereplicate the reads into amplicons. Then, it uses Medaka to polish the amplicons using the trimmed fastq files again.

Requirements
------------

This workflow requires that Canu and Medaka be available in the environment. Both Canu and Medaka can be installed via a Conda environment available here: https://gitlab.com/geneflow/conda/nanopore-amplicon-dereplication.git.

Canu must be run on a machine that has at least 4 CPU cores and 8 GB of RAM.

Inputs
------

The workflow requires one input file.

1. input: Sequence FASTQ file

Parameters
----------

Workflow parameters can be specified, but default values are used otherwise.

1. sampleName: Name of the sample, used to name the output directory. Default: sample

2. genomeSize: Estimated genome size. Default: 2000

3. minReadLength: Read shorter than this are discarded. Default: 750

4. minOverlapLength: Minimum overlap for contig-ing the reads. Default: 100

5. corMinCoverage: Controls the quality of the corrected reads. Default: 50

6. readSamplingCoverage: Data greater than this coverage number is ignored. Default: 25000

7. correctedErrorRate: Estimated Corrected Error Rate. 0.15 for nanopore R9. Default: 0.15

8. model: Sequence model to use for correction. Default: r941_min_high_g330

Detailed Installation Instructions
==================================

1. Clone the Conda environment repository into your home directory:

    .. code-block:: bash

        cd ~
        git clone https://gitlab.com/geneflow/conda/nanopore-amplicon-dereplication.git
        cd nanopore-amplicon-dereplication

2. If you don't already have Conda installed, you can install it by running the install script. Skip this step if Conda is already installed:

    .. code-block:: bash

        make install-miniconda
        source ~/.bashrc

3. If you don't already have GeneFlow installed, you can install it into a Conda environment using the Makefile:

    .. code-block:: bash

        make install-geneflow

    This installs a GeneFlow Conda environment in the "geneflow" directory.

4. Build the Conda environment for the Nanopore Amplicon Dereplication workflow:

    .. code-block:: bash

        make env-file

    This creates a Conda environment located in the "env" directory that contains Canu and Medaka. 

5. Install the Nanopore Amplicon Dereplication workflow. If you installed GeneFlow in a Conda environment in Step 3, you must activate the environment before running the workflow installation command and optionally deactivate it after installation finishes:

    .. code-block:: bash

        conda activate ~/nanopore-amplicon-dereplication/geneflow
        gf install-workflow -g https://gitlab.com/geneflow/workflows/nanopore-amplicon-dereplication-gf.git -c --make_apps ./workflow
        conda deactivate

    This installs the workflow in the "workflow" directory.

Run Instructions
================

1. If you installed GeneFlow in a Conda environment in Step 3 of the installation instructions, activate the environment:

    .. code-block:: bash

        conda activate ~/nanopore-amplicon-dereplication/geneflow

2. Activate the Conda environment for the Nanopore Amplicon Dereplication workflow:

    .. code-block:: bash

        conda activate --stack ~/nanopore-amplicon-dereplication/env

3. If you have Perl on your machine, it may conflict with the Perl in the Canu Conda environment. You can resolve the conflict by clearing the `PERL5LIB` environment variable:

    .. code-block:: bash

        PERL5LIB=

4. Run the workflow with the default parameters and packaged example data using the following command:

    .. code-block:: bash

        gf --log_level=debug run ~/nanopore-amplicon-dereplication/workflow \
            -d output_uri=~/geneflow-output \
            -d name=nanopore-amplicon-dereplication-test \
            -d no_output_hash \
            -d execution.method.default=environment \
            -d inputs.input=~/nanopore-amplicon-dereplication/workflow/data/sample.fastq.gz

Each command-line option of the GeneFlow run command can be explained as follows:

- output_uri=~/geneflow-output: The top-level output directory. This directory (`~/geneflow-output`) will be created if it doesn't already exist.
- name=nanopore-amplicon-dereplication-test: Name of the job. This is used as the name of the output sub-folder, which in this case is `nanopore-amplicon-dereplication-test`.
- no_output_hash: Do not add a unique hash string to the end of the output sub-folder.
- execution.method.default=environment: Execution method for all steps. When set to `environment`, workflow steps, i.e., canu and medaka, are expected to be in the environment PATH. Activating a valid Conda environment ensures that they are in the PATH.
- inputs.input=~/nanopore-amplicon-dereplication/workflow/data/sample.fastq.gz: Input sequence file to process with the workflow. This example sequence file is packaged with the workflow.

The output of this workflow run will be located in the directory ~/geneflow-output/nanopore-amplicaton-dereplication-test. Note that if you re-run the workflow, you'll need to change the `name` of the job. Otherwise, GeneFlow will try to write to the same output directory and fail.
